// source: v1/order/model_user_access.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
var v1_order_common_pb = require('../../v1/order/common_pb.js');
goog.object.extend(proto, v1_order_common_pb);
var v1_order_enum_pb = require('../../v1/order/enum_pb.js');
goog.object.extend(proto, v1_order_enum_pb);
goog.exportSymbol('proto.fcp.order.v1.order.GetUserAccessRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.GetUserAccessResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order.UserAccess', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.UserAccess = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.order.v1.order.UserAccess.repeatedFields_, null);
};
goog.inherits(proto.fcp.order.v1.order.UserAccess, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.UserAccess.displayName = 'proto.fcp.order.v1.order.UserAccess';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.GetUserAccessRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.GetUserAccessRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.GetUserAccessRequest.displayName = 'proto.fcp.order.v1.order.GetUserAccessRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order.GetUserAccessResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order.GetUserAccessResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order.GetUserAccessResponse.displayName = 'proto.fcp.order.v1.order.GetUserAccessResponse';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.order.v1.order.UserAccess.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.UserAccess.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.UserAccess.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.UserAccess} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.UserAccess.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    productProcess: jspb.Message.getFieldWithDefault(msg, 2, 0),
    productTypeList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    audit: (f = msg.getAudit()) && v1_order_common_pb.Audit.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.UserAccess}
 */
proto.fcp.order.v1.order.UserAccess.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.UserAccess;
  return proto.fcp.order.v1.order.UserAccess.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.UserAccess} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.UserAccess}
 */
proto.fcp.order.v1.order.UserAccess.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {!proto.fcp.order.v1.order.ProductProcess} */ (reader.readEnum());
      msg.setProductProcess(value);
      break;
    case 3:
      var value = /** @type {!Array<!proto.fcp.order.v1.order.ProductType>} */ (reader.readPackedEnum());
      msg.setProductTypeList(value);
      break;
    case 10:
      var value = new v1_order_common_pb.Audit;
      reader.readMessage(value,v1_order_common_pb.Audit.deserializeBinaryFromReader);
      msg.setAudit(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.UserAccess.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.UserAccess.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.UserAccess} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.UserAccess.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getProductProcess();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getProductTypeList();
  if (f.length > 0) {
    writer.writePackedEnum(
      3,
      f
    );
  }
  f = message.getAudit();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      v1_order_common_pb.Audit.serializeBinaryToWriter
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.order.v1.order.UserAccess.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.UserAccess} returns this
 */
proto.fcp.order.v1.order.UserAccess.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional ProductProcess product_process = 2;
 * @return {!proto.fcp.order.v1.order.ProductProcess}
 */
proto.fcp.order.v1.order.UserAccess.prototype.getProductProcess = function() {
  return /** @type {!proto.fcp.order.v1.order.ProductProcess} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.fcp.order.v1.order.ProductProcess} value
 * @return {!proto.fcp.order.v1.order.UserAccess} returns this
 */
proto.fcp.order.v1.order.UserAccess.prototype.setProductProcess = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * repeated ProductType product_type = 3;
 * @return {!Array<!proto.fcp.order.v1.order.ProductType>}
 */
proto.fcp.order.v1.order.UserAccess.prototype.getProductTypeList = function() {
  return /** @type {!Array<!proto.fcp.order.v1.order.ProductType>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<!proto.fcp.order.v1.order.ProductType>} value
 * @return {!proto.fcp.order.v1.order.UserAccess} returns this
 */
proto.fcp.order.v1.order.UserAccess.prototype.setProductTypeList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {!proto.fcp.order.v1.order.ProductType} value
 * @param {number=} opt_index
 * @return {!proto.fcp.order.v1.order.UserAccess} returns this
 */
proto.fcp.order.v1.order.UserAccess.prototype.addProductType = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.order.v1.order.UserAccess} returns this
 */
proto.fcp.order.v1.order.UserAccess.prototype.clearProductTypeList = function() {
  return this.setProductTypeList([]);
};


/**
 * optional Audit audit = 10;
 * @return {?proto.fcp.order.v1.order.Audit}
 */
proto.fcp.order.v1.order.UserAccess.prototype.getAudit = function() {
  return /** @type{?proto.fcp.order.v1.order.Audit} */ (
    jspb.Message.getWrapperField(this, v1_order_common_pb.Audit, 10));
};


/**
 * @param {?proto.fcp.order.v1.order.Audit|undefined} value
 * @return {!proto.fcp.order.v1.order.UserAccess} returns this
*/
proto.fcp.order.v1.order.UserAccess.prototype.setAudit = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.UserAccess} returns this
 */
proto.fcp.order.v1.order.UserAccess.prototype.clearAudit = function() {
  return this.setAudit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.UserAccess.prototype.hasAudit = function() {
  return jspb.Message.getField(this, 10) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.GetUserAccessRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.GetUserAccessRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.GetUserAccessRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetUserAccessRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.GetUserAccessRequest}
 */
proto.fcp.order.v1.order.GetUserAccessRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.GetUserAccessRequest;
  return proto.fcp.order.v1.order.GetUserAccessRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.GetUserAccessRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.GetUserAccessRequest}
 */
proto.fcp.order.v1.order.GetUserAccessRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.GetUserAccessRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.GetUserAccessRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.GetUserAccessRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetUserAccessRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.order.v1.order.GetUserAccessRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.order.v1.order.GetUserAccessRequest} returns this
 */
proto.fcp.order.v1.order.GetUserAccessRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order.GetUserAccessResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order.GetUserAccessResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order.GetUserAccessResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetUserAccessResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.order.v1.order.UserAccess.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order.GetUserAccessResponse}
 */
proto.fcp.order.v1.order.GetUserAccessResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order.GetUserAccessResponse;
  return proto.fcp.order.v1.order.GetUserAccessResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order.GetUserAccessResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order.GetUserAccessResponse}
 */
proto.fcp.order.v1.order.GetUserAccessResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.order.v1.order.UserAccess;
      reader.readMessage(value,proto.fcp.order.v1.order.UserAccess.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order.GetUserAccessResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order.GetUserAccessResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order.GetUserAccessResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order.GetUserAccessResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.order.v1.order.UserAccess.serializeBinaryToWriter
    );
  }
};


/**
 * optional UserAccess item = 1;
 * @return {?proto.fcp.order.v1.order.UserAccess}
 */
proto.fcp.order.v1.order.GetUserAccessResponse.prototype.getItem = function() {
  return /** @type{?proto.fcp.order.v1.order.UserAccess} */ (
    jspb.Message.getWrapperField(this, proto.fcp.order.v1.order.UserAccess, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.UserAccess|undefined} value
 * @return {!proto.fcp.order.v1.order.GetUserAccessResponse} returns this
*/
proto.fcp.order.v1.order.GetUserAccessResponse.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order.GetUserAccessResponse} returns this
 */
proto.fcp.order.v1.order.GetUserAccessResponse.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order.GetUserAccessResponse.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};


goog.object.extend(exports, proto.fcp.order.v1.order);
