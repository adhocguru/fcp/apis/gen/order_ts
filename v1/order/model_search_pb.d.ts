// package: fcp.order.v1.order
// file: v1/order/model_search.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_base_pb from "../../v1/order/model_base_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";
import * as v1_order_model_deal_pb from "../../v1/order/model_deal_pb";

export class SearchRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): SearchRequest.SearchFilter | undefined;
  setFilter(value?: SearchRequest.SearchFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SearchRequest): SearchRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchRequest;
  static deserializeBinaryFromReader(message: SearchRequest, reader: jspb.BinaryReader): SearchRequest;
}

export namespace SearchRequest {
  export type AsObject = {
    filter?: SearchRequest.SearchFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }

  export class SearchFilter extends jspb.Message {
    hasSearchString(): boolean;
    clearSearchString(): void;
    getSearchString(): google_protobuf_wrappers_pb.StringValue | undefined;
    setSearchString(value?: google_protobuf_wrappers_pb.StringValue): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SearchFilter.AsObject;
    static toObject(includeInstance: boolean, msg: SearchFilter): SearchFilter.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SearchFilter, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SearchFilter;
    static deserializeBinaryFromReader(message: SearchFilter, reader: jspb.BinaryReader): SearchFilter;
  }

  export namespace SearchFilter {
    export type AsObject = {
      searchString?: google_protobuf_wrappers_pb.StringValue.AsObject,
    }
  }
}

export class SearchResponse extends jspb.Message {
  hasOrders(): boolean;
  clearOrders(): void;
  getOrders(): v1_order_model_order_pb.ListOrderResponse | undefined;
  setOrders(value?: v1_order_model_order_pb.ListOrderResponse): void;

  hasOffers(): boolean;
  clearOffers(): void;
  getOffers(): v1_order_model_offer_pb.ListOfferResponse | undefined;
  setOffers(value?: v1_order_model_offer_pb.ListOfferResponse): void;

  hasDeals(): boolean;
  clearDeals(): void;
  getDeals(): v1_order_model_deal_pb.ListDealResponse | undefined;
  setDeals(value?: v1_order_model_deal_pb.ListDealResponse): void;

  hasContacts(): boolean;
  clearContacts(): void;
  getContacts(): v1_order_model_deal_pb.ListDealContactResponse | undefined;
  setContacts(value?: v1_order_model_deal_pb.ListDealContactResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SearchResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SearchResponse): SearchResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SearchResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SearchResponse;
  static deserializeBinaryFromReader(message: SearchResponse, reader: jspb.BinaryReader): SearchResponse;
}

export namespace SearchResponse {
  export type AsObject = {
    orders?: v1_order_model_order_pb.ListOrderResponse.AsObject,
    offers?: v1_order_model_offer_pb.ListOfferResponse.AsObject,
    deals?: v1_order_model_deal_pb.ListDealResponse.AsObject,
    contacts?: v1_order_model_deal_pb.ListDealContactResponse.AsObject,
  }
}

