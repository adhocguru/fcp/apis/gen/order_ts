// package: fcp.order.v1.order
// file: v1/order/model_user_access.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";

export class UserAccess extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getProductProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProductProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  clearProductTypeList(): void;
  getProductTypeList(): Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>;
  setProductTypeList(value: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>): void;
  addProductType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap], index?: number): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserAccess.AsObject;
  static toObject(includeInstance: boolean, msg: UserAccess): UserAccess.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserAccess, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserAccess;
  static deserializeBinaryFromReader(message: UserAccess, reader: jspb.BinaryReader): UserAccess;
}

export namespace UserAccess {
  export type AsObject = {
    userId: string,
    productProcess: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    productTypeList: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>,
    audit?: v1_order_common_pb.Audit.AsObject,
  }
}

export class GetUserAccessRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserAccessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserAccessRequest): GetUserAccessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserAccessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserAccessRequest;
  static deserializeBinaryFromReader(message: GetUserAccessRequest, reader: jspb.BinaryReader): GetUserAccessRequest;
}

export namespace GetUserAccessRequest {
  export type AsObject = {
    userId: string,
  }
}

export class GetUserAccessResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): UserAccess | undefined;
  setItem(value?: UserAccess): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserAccessResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserAccessResponse): GetUserAccessResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserAccessResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserAccessResponse;
  static deserializeBinaryFromReader(message: GetUserAccessResponse, reader: jspb.BinaryReader): GetUserAccessResponse;
}

export namespace GetUserAccessResponse {
  export type AsObject = {
    item?: UserAccess.AsObject,
  }
}

