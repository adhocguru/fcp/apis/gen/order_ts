// package: fcp.order.v1.order
// file: v1/order/model_offer.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_finance_pb from "../../v1/order/finance_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_file_pb from "../../v1/order/file_pb";
import * as v1_order_model_base_pb from "../../v1/order/model_base_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";

export class OfferCondition extends jspb.Message {
  getDeliveryCondition(): v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap];
  setDeliveryCondition(value: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]): void;

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDeliveryDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getDeliveryPlace(): string;
  setDeliveryPlace(value: string): void;

  getPaymentCondition(): v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap];
  setPaymentCondition(value: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]): void;

  hasPaymentDate(): boolean;
  clearPaymentDate(): void;
  getPaymentDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setPaymentDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasExpiredDate(): boolean;
  clearExpiredDate(): void;
  getExpiredDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setExpiredDate(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OfferCondition.AsObject;
  static toObject(includeInstance: boolean, msg: OfferCondition): OfferCondition.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OfferCondition, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OfferCondition;
  static deserializeBinaryFromReader(message: OfferCondition, reader: jspb.BinaryReader): OfferCondition;
}

export namespace OfferCondition {
  export type AsObject = {
    deliveryCondition: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap],
    deliveryDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    deliveryPlace: string,
    paymentCondition: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap],
    paymentDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    expiredDate?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class OfferConditionFilter extends jspb.Message {
  clearDeliveryConditionList(): void;
  getDeliveryConditionList(): Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>;
  setDeliveryConditionList(value: Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>): void;
  addDeliveryCondition(value: v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap], index?: number): v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap];

  hasDeliveryDate(): boolean;
  clearDeliveryDate(): void;
  getDeliveryDate(): v1_order_common_pb.TimeRange | undefined;
  setDeliveryDate(value?: v1_order_common_pb.TimeRange): void;

  clearPaymentConditionList(): void;
  getPaymentConditionList(): Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>;
  setPaymentConditionList(value: Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>): void;
  addPaymentCondition(value: v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap], index?: number): v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap];

  hasPaymentDate(): boolean;
  clearPaymentDate(): void;
  getPaymentDate(): v1_order_common_pb.TimeRange | undefined;
  setPaymentDate(value?: v1_order_common_pb.TimeRange): void;

  hasExpiredDate(): boolean;
  clearExpiredDate(): void;
  getExpiredDate(): v1_order_common_pb.TimeRange | undefined;
  setExpiredDate(value?: v1_order_common_pb.TimeRange): void;

  hasAlternatives(): boolean;
  clearAlternatives(): void;
  getAlternatives(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setAlternatives(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasConsulting(): boolean;
  clearConsulting(): void;
  getConsulting(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setConsulting(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OfferConditionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: OfferConditionFilter): OfferConditionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OfferConditionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OfferConditionFilter;
  static deserializeBinaryFromReader(message: OfferConditionFilter, reader: jspb.BinaryReader): OfferConditionFilter;
}

export namespace OfferConditionFilter {
  export type AsObject = {
    deliveryConditionList: Array<v1_order_enum_pb.DeliveryConditionMap[keyof v1_order_enum_pb.DeliveryConditionMap]>,
    deliveryDate?: v1_order_common_pb.TimeRange.AsObject,
    paymentConditionList: Array<v1_order_enum_pb.PaymentConditionMap[keyof v1_order_enum_pb.PaymentConditionMap]>,
    paymentDate?: v1_order_common_pb.TimeRange.AsObject,
    expiredDate?: v1_order_common_pb.TimeRange.AsObject,
    alternatives?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    consulting?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class OfferResult extends jspb.Message {
  hasDeal(): boolean;
  clearDeal(): void;
  getDeal(): v1_order_model_base_pb.OperationIdent | undefined;
  setDeal(value?: v1_order_model_base_pb.OperationIdent): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OfferResult.AsObject;
  static toObject(includeInstance: boolean, msg: OfferResult): OfferResult.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OfferResult, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OfferResult;
  static deserializeBinaryFromReader(message: OfferResult, reader: jspb.BinaryReader): OfferResult;
}

export namespace OfferResult {
  export type AsObject = {
    deal?: v1_order_model_base_pb.OperationIdent.AsObject,
  }
}

export class Offer extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getStatus(): Offer.StatusMap[keyof Offer.StatusMap];
  setStatus(value: Offer.StatusMap[keyof Offer.StatusMap]): void;

  hasOrder(): boolean;
  clearOrder(): void;
  getOrder(): v1_order_model_order_pb.Order | undefined;
  setOrder(value?: v1_order_model_order_pb.Order): void;

  hasProduct(): boolean;
  clearProduct(): void;
  getProduct(): v1_order_model_product_pb.Product | undefined;
  setProduct(value?: v1_order_model_product_pb.Product): void;

  getCurrency(): string;
  setCurrency(value: string): void;

  getPrice(): number;
  setPrice(value: number): void;

  hasDiscount(): boolean;
  clearDiscount(): void;
  getDiscount(): v1_order_finance_pb.Discount | undefined;
  setDiscount(value?: v1_order_finance_pb.Discount): void;

  getDiscountAmount(): number;
  setDiscountAmount(value: number): void;

  getTotalAmount(): number;
  setTotalAmount(value: number): void;

  hasCondition(): boolean;
  clearCondition(): void;
  getCondition(): OfferCondition | undefined;
  setCondition(value?: OfferCondition): void;

  clearOfferFilesList(): void;
  getOfferFilesList(): Array<v1_order_file_pb.File>;
  setOfferFilesList(value: Array<v1_order_file_pb.File>): void;
  addOfferFiles(value?: v1_order_file_pb.File, index?: number): v1_order_file_pb.File;

  hasResult(): boolean;
  clearResult(): void;
  getResult(): OfferResult | undefined;
  setResult(value?: OfferResult): void;

  getAdditionalConditions(): string;
  setAdditionalConditions(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_order_common_pb.Audit | undefined;
  setAudit(value?: v1_order_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Offer.AsObject;
  static toObject(includeInstance: boolean, msg: Offer): Offer.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Offer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Offer;
  static deserializeBinaryFromReader(message: Offer, reader: jspb.BinaryReader): Offer;
}

export namespace Offer {
  export type AsObject = {
    id: number,
    name: string,
    status: Offer.StatusMap[keyof Offer.StatusMap],
    order?: v1_order_model_order_pb.Order.AsObject,
    product?: v1_order_model_product_pb.Product.AsObject,
    currency: string,
    price: number,
    discount?: v1_order_finance_pb.Discount.AsObject,
    discountAmount: number,
    totalAmount: number,
    condition?: OfferCondition.AsObject,
    offerFilesList: Array<v1_order_file_pb.File.AsObject>,
    result?: OfferResult.AsObject,
    additionalConditions: string,
    description: string,
    audit?: v1_order_common_pb.Audit.AsObject,
  }

  export class StatusValue extends jspb.Message {
    getValue(): Offer.StatusMap[keyof Offer.StatusMap];
    setValue(value: Offer.StatusMap[keyof Offer.StatusMap]): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StatusValue.AsObject;
    static toObject(includeInstance: boolean, msg: StatusValue): StatusValue.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StatusValue, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StatusValue;
    static deserializeBinaryFromReader(message: StatusValue, reader: jspb.BinaryReader): StatusValue;
  }

  export namespace StatusValue {
    export type AsObject = {
      value: Offer.StatusMap[keyof Offer.StatusMap],
    }
  }

  export interface StatusMap {
    ACTIVE: 0;
    REJECT: 1;
    DONE: 2;
    DRAFT: 3;
  }

  export const Status: StatusMap;
}

export class OfferFilter extends jspb.Message {
  hasOrderId(): boolean;
  clearOrderId(): void;
  getOrderId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setOrderId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasStatus(): boolean;
  clearStatus(): void;
  getStatus(): Offer.StatusValue | undefined;
  setStatus(value?: Offer.StatusValue): void;

  hasProductFilter(): boolean;
  clearProductFilter(): void;
  getProductFilter(): v1_order_model_product_pb.ProductFilter | undefined;
  setProductFilter(value?: v1_order_model_product_pb.ProductFilter): void;

  hasConditionFilter(): boolean;
  clearConditionFilter(): void;
  getConditionFilter(): OfferConditionFilter | undefined;
  setConditionFilter(value?: OfferConditionFilter): void;

  hasDescription(): boolean;
  clearDescription(): void;
  getDescription(): google_protobuf_wrappers_pb.StringValue | undefined;
  setDescription(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_order_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_order_common_pb.TimeRange): void;

  hasCreatedName(): boolean;
  clearCreatedName(): void;
  getCreatedName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_order_common_pb.TimeRange | undefined;
  setCreated(value?: v1_order_common_pb.TimeRange): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OfferFilter.AsObject;
  static toObject(includeInstance: boolean, msg: OfferFilter): OfferFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OfferFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OfferFilter;
  static deserializeBinaryFromReader(message: OfferFilter, reader: jspb.BinaryReader): OfferFilter;
}

export namespace OfferFilter {
  export type AsObject = {
    orderId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    status?: Offer.StatusValue.AsObject,
    productFilter?: v1_order_model_product_pb.ProductFilter.AsObject,
    conditionFilter?: OfferConditionFilter.AsObject,
    description?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updated?: v1_order_common_pb.TimeRange.AsObject,
    createdName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_order_common_pb.TimeRange.AsObject,
  }
}

export class ListOfferRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferRequest): ListOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferRequest;
  static deserializeBinaryFromReader(message: ListOfferRequest, reader: jspb.BinaryReader): ListOfferRequest;
}

export namespace ListOfferRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListOfferResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Offer>;
  setItemsList(value: Array<Offer>): void;
  addItems(value?: Offer, index?: number): Offer;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferResponse): ListOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferResponse;
  static deserializeBinaryFromReader(message: ListOfferResponse, reader: jspb.BinaryReader): ListOfferResponse;
}

export namespace ListOfferResponse {
  export type AsObject = {
    itemsList: Array<Offer.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class GetOfferRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetOfferRequest): GetOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOfferRequest;
  static deserializeBinaryFromReader(message: GetOfferRequest, reader: jspb.BinaryReader): GetOfferRequest;
}

export namespace GetOfferRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetOfferResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Offer | undefined;
  setItem(value?: Offer): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetOfferResponse): GetOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOfferResponse;
  static deserializeBinaryFromReader(message: GetOfferResponse, reader: jspb.BinaryReader): GetOfferResponse;
}

export namespace GetOfferResponse {
  export type AsObject = {
    item?: Offer.AsObject,
  }
}

export class GetOfferFileRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOfferFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetOfferFileRequest): GetOfferFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOfferFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOfferFileRequest;
  static deserializeBinaryFromReader(message: GetOfferFileRequest, reader: jspb.BinaryReader): GetOfferFileRequest;
}

export namespace GetOfferFileRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetOfferFileResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_file_pb.File | undefined;
  setItem(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOfferFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetOfferFileResponse): GetOfferFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOfferFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOfferFileResponse;
  static deserializeBinaryFromReader(message: GetOfferFileResponse, reader: jspb.BinaryReader): GetOfferFileResponse;
}

export namespace GetOfferFileResponse {
  export type AsObject = {
    item?: v1_order_file_pb.File.AsObject,
  }
}

export class ListOfferFileRequest extends jspb.Message {
  getOfferId(): number;
  setOfferId(value: number): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_order_common_pb.Sorting>;
  setSortingList(value: Array<v1_order_common_pb.Sorting>): void;
  addSorting(value?: v1_order_common_pb.Sorting, index?: number): v1_order_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_order_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferFileRequest): ListOfferFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferFileRequest;
  static deserializeBinaryFromReader(message: ListOfferFileRequest, reader: jspb.BinaryReader): ListOfferFileRequest;
}

export namespace ListOfferFileRequest {
  export type AsObject = {
    offerId: number,
    sortingList: Array<v1_order_common_pb.Sorting.AsObject>,
    pagination?: v1_order_common_pb.PaginationRequest.AsObject,
  }
}

export class ListOfferFileResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_order_file_pb.File>;
  setItemsList(value: Array<v1_order_file_pb.File>): void;
  addItems(value?: v1_order_file_pb.File, index?: number): v1_order_file_pb.File;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_order_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_order_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferFileResponse): ListOfferFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferFileResponse;
  static deserializeBinaryFromReader(message: ListOfferFileResponse, reader: jspb.BinaryReader): ListOfferFileResponse;
}

export namespace ListOfferFileResponse {
  export type AsObject = {
    itemsList: Array<v1_order_file_pb.File.AsObject>,
    pagination?: v1_order_common_pb.PaginationResponse.AsObject,
  }
}

export class AddOfferFileRequest extends jspb.Message {
  getOfferId(): number;
  setOfferId(value: number): void;

  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddOfferFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AddOfferFileRequest): AddOfferFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddOfferFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddOfferFileRequest;
  static deserializeBinaryFromReader(message: AddOfferFileRequest, reader: jspb.BinaryReader): AddOfferFileRequest;
}

export namespace AddOfferFileRequest {
  export type AsObject = {
    offerId: number,
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class AddOfferFileResponse extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddOfferFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AddOfferFileResponse): AddOfferFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddOfferFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddOfferFileResponse;
  static deserializeBinaryFromReader(message: AddOfferFileResponse, reader: jspb.BinaryReader): AddOfferFileResponse;
}

export namespace AddOfferFileResponse {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteOfferFileRequest extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_order_file_pb.File | undefined;
  setFile(value?: v1_order_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteOfferFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteOfferFileRequest): DeleteOfferFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteOfferFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteOfferFileRequest;
  static deserializeBinaryFromReader(message: DeleteOfferFileRequest, reader: jspb.BinaryReader): DeleteOfferFileRequest;
}

export namespace DeleteOfferFileRequest {
  export type AsObject = {
    file?: v1_order_file_pb.File.AsObject,
  }
}

export class DeleteOfferFileResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteOfferFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteOfferFileResponse): DeleteOfferFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteOfferFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteOfferFileResponse;
  static deserializeBinaryFromReader(message: DeleteOfferFileResponse, reader: jspb.BinaryReader): DeleteOfferFileResponse;
}

export namespace DeleteOfferFileResponse {
  export type AsObject = {
  }
}

export class ListOfferProcessRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferProcessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferProcessRequest): ListOfferProcessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferProcessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferProcessRequest;
  static deserializeBinaryFromReader(message: ListOfferProcessRequest, reader: jspb.BinaryReader): ListOfferProcessRequest;
}

export namespace ListOfferProcessRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferProcessTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferProcessTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferProcessTypeRequest): ListOfferProcessTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferProcessTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferProcessTypeRequest;
  static deserializeBinaryFromReader(message: ListOfferProcessTypeRequest, reader: jspb.BinaryReader): ListOfferProcessTypeRequest;
}

export namespace ListOfferProcessTypeRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferDeliveryConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferDeliveryConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferDeliveryConditionRequest): ListOfferDeliveryConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferDeliveryConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferDeliveryConditionRequest;
  static deserializeBinaryFromReader(message: ListOfferDeliveryConditionRequest, reader: jspb.BinaryReader): ListOfferDeliveryConditionRequest;
}

export namespace ListOfferDeliveryConditionRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferPaymentConditionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferPaymentConditionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferPaymentConditionRequest): ListOfferPaymentConditionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferPaymentConditionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferPaymentConditionRequest;
  static deserializeBinaryFromReader(message: ListOfferPaymentConditionRequest, reader: jspb.BinaryReader): ListOfferPaymentConditionRequest;
}

export namespace ListOfferPaymentConditionRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferCategoryRequest): ListOfferCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferCategoryRequest;
  static deserializeBinaryFromReader(message: ListOfferCategoryRequest, reader: jspb.BinaryReader): ListOfferCategoryRequest;
}

export namespace ListOfferCategoryRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferBrandRequest): ListOfferBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferBrandRequest;
  static deserializeBinaryFromReader(message: ListOfferBrandRequest, reader: jspb.BinaryReader): ListOfferBrandRequest;
}

export namespace ListOfferBrandRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferProductRequest): ListOfferProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferProductRequest;
  static deserializeBinaryFromReader(message: ListOfferProductRequest, reader: jspb.BinaryReader): ListOfferProductRequest;
}

export namespace ListOfferProductRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OfferFilter | undefined;
  setFilter(value?: OfferFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferQuantityTypeRequest): ListOfferQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListOfferQuantityTypeRequest, reader: jspb.BinaryReader): ListOfferQuantityTypeRequest;
}

export namespace ListOfferQuantityTypeRequest {
  export type AsObject = {
    filter?: OfferFilter.AsObject,
  }
}

export class ListOfferNamesRequest extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.NameFilter>;
  setProcessesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProcesses(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.NameFilter>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addPaymentConditions(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.NameFilter>;
  setCategoriesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addCategories(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.NameFilter>;
  setBrandsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addBrands(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.NameFilter>;
  setProductsList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addProducts(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.NameFilter>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.NameFilter>): void;
  addQuantityTypes(value?: v1_order_enum_pb.NameFilter, index?: number): v1_order_enum_pb.NameFilter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferNamesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferNamesRequest): ListOfferNamesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferNamesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferNamesRequest;
  static deserializeBinaryFromReader(message: ListOfferNamesRequest, reader: jspb.BinaryReader): ListOfferNamesRequest;
}

export namespace ListOfferNamesRequest {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    typesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    categoriesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    brandsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    productsList: Array<v1_order_enum_pb.NameFilter.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.NameFilter.AsObject>,
  }
}

export class ListOfferNamesResponse extends jspb.Message {
  clearProcessesList(): void;
  getProcessesList(): Array<v1_order_enum_pb.DictItem>;
  setProcessesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProcesses(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearTypesList(): void;
  getTypesList(): Array<v1_order_enum_pb.DictItem>;
  setTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearDeliveryConditionsList(): void;
  getDeliveryConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setDeliveryConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addDeliveryConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearPaymentConditionsList(): void;
  getPaymentConditionsList(): Array<v1_order_enum_pb.DictItem>;
  setPaymentConditionsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addPaymentConditions(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_order_enum_pb.DictItem>;
  setCategoriesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addCategories(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearBrandsList(): void;
  getBrandsList(): Array<v1_order_enum_pb.DictItem>;
  setBrandsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addBrands(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearProductsList(): void;
  getProductsList(): Array<v1_order_enum_pb.DictItem>;
  setProductsList(value: Array<v1_order_enum_pb.DictItem>): void;
  addProducts(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_order_enum_pb.DictItem>;
  setQuantityTypesList(value: Array<v1_order_enum_pb.DictItem>): void;
  addQuantityTypes(value?: v1_order_enum_pb.DictItem, index?: number): v1_order_enum_pb.DictItem;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOfferNamesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOfferNamesResponse): ListOfferNamesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOfferNamesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOfferNamesResponse;
  static deserializeBinaryFromReader(message: ListOfferNamesResponse, reader: jspb.BinaryReader): ListOfferNamesResponse;
}

export namespace ListOfferNamesResponse {
  export type AsObject = {
    processesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    typesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    deliveryConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    paymentConditionsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    categoriesList: Array<v1_order_enum_pb.DictItem.AsObject>,
    brandsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    productsList: Array<v1_order_enum_pb.DictItem.AsObject>,
    quantityTypesList: Array<v1_order_enum_pb.DictItem.AsObject>,
  }
}

