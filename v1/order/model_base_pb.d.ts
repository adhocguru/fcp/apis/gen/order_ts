// package: fcp.order.v1.order
// file: v1/order/model_base.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_order_common_pb from "../../v1/order/common_pb";

export class OperationIdent extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  hasTime(): boolean;
  clearTime(): void;
  getTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_order_common_pb.User | undefined;
  setUser(value?: v1_order_common_pb.User): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OperationIdent.AsObject;
  static toObject(includeInstance: boolean, msg: OperationIdent): OperationIdent.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OperationIdent, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OperationIdent;
  static deserializeBinaryFromReader(message: OperationIdent, reader: jspb.BinaryReader): OperationIdent;
}

export namespace OperationIdent {
  export type AsObject = {
    id: number,
    time?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    user?: v1_order_common_pb.User.AsObject,
  }
}

export class UserInfo extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getStatus(): number;
  setStatus(value: number): void;

  getLogin(): string;
  setLogin(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getRole(): string;
  setRole(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  clearContactsList(): void;
  getContactsList(): Array<string>;
  setContactsList(value: Array<string>): void;
  addContacts(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserInfo.AsObject;
  static toObject(includeInstance: boolean, msg: UserInfo): UserInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserInfo;
  static deserializeBinaryFromReader(message: UserInfo, reader: jspb.BinaryReader): UserInfo;
}

export namespace UserInfo {
  export type AsObject = {
    id: string,
    status: number,
    login: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    role: string,
    businessName: string,
    businessAddress: string,
    contactsList: Array<string>,
  }
}

