// package: fcp.order.v1.order
// file: v1/order/model_product.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";

export class Product extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getProcess(): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];
  setProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]): void;

  getType(): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];
  setType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]): void;

  getCategoryId(): number;
  setCategoryId(value: number): void;

  getCategoryName(): string;
  setCategoryName(value: string): void;

  getBrandId(): number;
  setBrandId(value: number): void;

  getBrandName(): string;
  setBrandName(value: string): void;

  getQuantityTypeId(): number;
  setQuantityTypeId(value: number): void;

  getQuantityTypeName(): string;
  setQuantityTypeName(value: string): void;

  getQuantity(): number;
  setQuantity(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Product.AsObject;
  static toObject(includeInstance: boolean, msg: Product): Product.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Product, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Product;
  static deserializeBinaryFromReader(message: Product, reader: jspb.BinaryReader): Product;
}

export namespace Product {
  export type AsObject = {
    id: number,
    name: string,
    process: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap],
    type: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap],
    categoryId: number,
    categoryName: string,
    brandId: number,
    brandName: string,
    quantityTypeId: number,
    quantityTypeName: string,
    quantity: number,
  }
}

export class ProductFilter extends jspb.Message {
  clearIdList(): void;
  getIdList(): Array<number>;
  setIdList(value: Array<number>): void;
  addId(value: number, index?: number): number;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearProcessList(): void;
  getProcessList(): Array<v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]>;
  setProcessList(value: Array<v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]>): void;
  addProcess(value: v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap], index?: number): v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap];

  clearTypeList(): void;
  getTypeList(): Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>;
  setTypeList(value: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>): void;
  addType(value: v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap], index?: number): v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap];

  clearCategoryIdList(): void;
  getCategoryIdList(): Array<number>;
  setCategoryIdList(value: Array<number>): void;
  addCategoryId(value: number, index?: number): number;

  hasCategoryName(): boolean;
  clearCategoryName(): void;
  getCategoryName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCategoryName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearBrandIdList(): void;
  getBrandIdList(): Array<number>;
  setBrandIdList(value: Array<number>): void;
  addBrandId(value: number, index?: number): number;

  hasBrandName(): boolean;
  clearBrandName(): void;
  getBrandName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setBrandName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearQuantityTypeIdList(): void;
  getQuantityTypeIdList(): Array<number>;
  setQuantityTypeIdList(value: Array<number>): void;
  addQuantityTypeId(value: number, index?: number): number;

  hasQuantityTypeName(): boolean;
  clearQuantityTypeName(): void;
  getQuantityTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setQuantityTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFilter): ProductFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFilter;
  static deserializeBinaryFromReader(message: ProductFilter, reader: jspb.BinaryReader): ProductFilter;
}

export namespace ProductFilter {
  export type AsObject = {
    idList: Array<number>,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    processList: Array<v1_order_enum_pb.ProductProcessMap[keyof v1_order_enum_pb.ProductProcessMap]>,
    typeList: Array<v1_order_enum_pb.ProductTypeMap[keyof v1_order_enum_pb.ProductTypeMap]>,
    categoryIdList: Array<number>,
    categoryName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    brandIdList: Array<number>,
    brandName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    quantityTypeIdList: Array<number>,
    quantityTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ResourcesSeeds extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesSeeds.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesSeeds): ResourcesSeeds.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesSeeds, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesSeeds;
  static deserializeBinaryFromReader(message: ResourcesSeeds, reader: jspb.BinaryReader): ResourcesSeeds;
}

export namespace ResourcesSeeds {
  export type AsObject = {
  }
}

export class ResourcesFertilize extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesFertilize.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesFertilize): ResourcesFertilize.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesFertilize, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesFertilize;
  static deserializeBinaryFromReader(message: ResourcesFertilize, reader: jspb.BinaryReader): ResourcesFertilize;
}

export namespace ResourcesFertilize {
  export type AsObject = {
  }
}

export class ResourcesMicroFertilizers extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesMicroFertilizers.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesMicroFertilizers): ResourcesMicroFertilizers.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesMicroFertilizers, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesMicroFertilizers;
  static deserializeBinaryFromReader(message: ResourcesMicroFertilizers, reader: jspb.BinaryReader): ResourcesMicroFertilizers;
}

export namespace ResourcesMicroFertilizers {
  export type AsObject = {
  }
}

export class ResourcesChemistry extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResourcesChemistry.AsObject;
  static toObject(includeInstance: boolean, msg: ResourcesChemistry): ResourcesChemistry.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResourcesChemistry, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResourcesChemistry;
  static deserializeBinaryFromReader(message: ResourcesChemistry, reader: jspb.BinaryReader): ResourcesChemistry;
}

export namespace ResourcesChemistry {
  export type AsObject = {
  }
}

