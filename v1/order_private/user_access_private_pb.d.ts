// package: fcp.order.v1.order_private
// file: v1/order_private/user_access_private.proto

import * as jspb from "google-protobuf";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_user_access_pb from "../../v1/order/model_user_access_pb";

export class SaveUserAccessRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_model_user_access_pb.UserAccess | undefined;
  setItem(value?: v1_order_model_user_access_pb.UserAccess): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveUserAccessRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveUserAccessRequest): SaveUserAccessRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveUserAccessRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveUserAccessRequest;
  static deserializeBinaryFromReader(message: SaveUserAccessRequest, reader: jspb.BinaryReader): SaveUserAccessRequest;
}

export namespace SaveUserAccessRequest {
  export type AsObject = {
    item?: v1_order_model_user_access_pb.UserAccess.AsObject,
  }
}

export class SaveUserAccessResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveUserAccessResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveUserAccessResponse): SaveUserAccessResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveUserAccessResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveUserAccessResponse;
  static deserializeBinaryFromReader(message: SaveUserAccessResponse, reader: jspb.BinaryReader): SaveUserAccessResponse;
}

export namespace SaveUserAccessResponse {
  export type AsObject = {
  }
}

