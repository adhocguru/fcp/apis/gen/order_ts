// package: fcp.order.v1.order_web
// file: v1/order_web/offer_web.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";

export class SaveOfferRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_model_offer_pb.Offer | undefined;
  setItem(value?: v1_order_model_offer_pb.Offer): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOfferRequest): SaveOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOfferRequest;
  static deserializeBinaryFromReader(message: SaveOfferRequest, reader: jspb.BinaryReader): SaveOfferRequest;
}

export namespace SaveOfferRequest {
  export type AsObject = {
    item?: v1_order_model_offer_pb.Offer.AsObject,
  }
}

export class SaveOfferResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOfferResponse): SaveOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOfferResponse;
  static deserializeBinaryFromReader(message: SaveOfferResponse, reader: jspb.BinaryReader): SaveOfferResponse;
}

export namespace SaveOfferResponse {
  export type AsObject = {
    id: number,
  }
}

export class ActivateOfferRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActivateOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ActivateOfferRequest): ActivateOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActivateOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActivateOfferRequest;
  static deserializeBinaryFromReader(message: ActivateOfferRequest, reader: jspb.BinaryReader): ActivateOfferRequest;
}

export namespace ActivateOfferRequest {
  export type AsObject = {
    id: number,
  }
}

export class ActivateOfferResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActivateOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ActivateOfferResponse): ActivateOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActivateOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActivateOfferResponse;
  static deserializeBinaryFromReader(message: ActivateOfferResponse, reader: jspb.BinaryReader): ActivateOfferResponse;
}

export namespace ActivateOfferResponse {
  export type AsObject = {
  }
}

export class RejectOfferRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectOfferRequest): RejectOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectOfferRequest;
  static deserializeBinaryFromReader(message: RejectOfferRequest, reader: jspb.BinaryReader): RejectOfferRequest;
}

export namespace RejectOfferRequest {
  export type AsObject = {
    id: number,
  }
}

export class RejectOfferResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectOfferResponse): RejectOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectOfferResponse;
  static deserializeBinaryFromReader(message: RejectOfferResponse, reader: jspb.BinaryReader): RejectOfferResponse;
}

export namespace RejectOfferResponse {
  export type AsObject = {
  }
}

