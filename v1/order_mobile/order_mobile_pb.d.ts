// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/order_mobile.proto

import * as jspb from "google-protobuf";
import * as v1_order_common_pb from "../../v1/order/common_pb";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_order_pb from "../../v1/order/model_order_pb";
import * as v1_order_model_product_pb from "../../v1/order/model_product_pb";

export class SaveOrderRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_order_model_order_pb.Order | undefined;
  setItem(value?: v1_order_model_order_pb.Order): void;

  hasResourcesSeeds(): boolean;
  clearResourcesSeeds(): void;
  getResourcesSeeds(): v1_order_model_product_pb.ResourcesSeeds | undefined;
  setResourcesSeeds(value?: v1_order_model_product_pb.ResourcesSeeds): void;

  hasResourcesFertilize(): boolean;
  clearResourcesFertilize(): void;
  getResourcesFertilize(): v1_order_model_product_pb.ResourcesFertilize | undefined;
  setResourcesFertilize(value?: v1_order_model_product_pb.ResourcesFertilize): void;

  hasResourcesMicroFertilize(): boolean;
  clearResourcesMicroFertilize(): void;
  getResourcesMicroFertilize(): v1_order_model_product_pb.ResourcesMicroFertilizers | undefined;
  setResourcesMicroFertilize(value?: v1_order_model_product_pb.ResourcesMicroFertilizers): void;

  hasResourcesChemistry(): boolean;
  clearResourcesChemistry(): void;
  getResourcesChemistry(): v1_order_model_product_pb.ResourcesChemistry | undefined;
  setResourcesChemistry(value?: v1_order_model_product_pb.ResourcesChemistry): void;

  getDetailCase(): SaveOrderRequest.DetailCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOrderRequest): SaveOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOrderRequest;
  static deserializeBinaryFromReader(message: SaveOrderRequest, reader: jspb.BinaryReader): SaveOrderRequest;
}

export namespace SaveOrderRequest {
  export type AsObject = {
    item?: v1_order_model_order_pb.Order.AsObject,
    resourcesSeeds?: v1_order_model_product_pb.ResourcesSeeds.AsObject,
    resourcesFertilize?: v1_order_model_product_pb.ResourcesFertilize.AsObject,
    resourcesMicroFertilize?: v1_order_model_product_pb.ResourcesMicroFertilizers.AsObject,
    resourcesChemistry?: v1_order_model_product_pb.ResourcesChemistry.AsObject,
  }

  export enum DetailCase {
    DETAIL_NOT_SET = 0,
    RESOURCES_SEEDS = 100,
    RESOURCES_FERTILIZE = 101,
    RESOURCES_MICRO_FERTILIZE = 102,
    RESOURCES_CHEMISTRY = 103,
  }
}

export class SaveOrderResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOrderResponse): SaveOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOrderResponse;
  static deserializeBinaryFromReader(message: SaveOrderResponse, reader: jspb.BinaryReader): SaveOrderResponse;
}

export namespace SaveOrderResponse {
  export type AsObject = {
    id: number,
  }
}

export class RejectOrderRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectOrderRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RejectOrderRequest): RejectOrderRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectOrderRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectOrderRequest;
  static deserializeBinaryFromReader(message: RejectOrderRequest, reader: jspb.BinaryReader): RejectOrderRequest;
}

export namespace RejectOrderRequest {
  export type AsObject = {
    id: number,
  }
}

export class RejectOrderResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RejectOrderResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RejectOrderResponse): RejectOrderResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RejectOrderResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RejectOrderResponse;
  static deserializeBinaryFromReader(message: RejectOrderResponse, reader: jspb.BinaryReader): RejectOrderResponse;
}

export namespace RejectOrderResponse {
  export type AsObject = {
  }
}

