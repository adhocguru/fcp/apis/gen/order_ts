// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/search_mobile.proto

var v1_order_mobile_search_mobile_pb = require("../../v1/order_mobile/search_mobile_pb");
var v1_order_model_search_pb = require("../../v1/order/model_search_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var SearchService = (function () {
  function SearchService() {}
  SearchService.serviceName = "fcp.order.v1.order_mobile.SearchService";
  return SearchService;
}());

SearchService.Search = {
  methodName: "Search",
  service: SearchService,
  requestStream: false,
  responseStream: false,
  requestType: v1_order_model_search_pb.SearchRequest,
  responseType: v1_order_model_search_pb.SearchResponse
};

exports.SearchService = SearchService;

function SearchServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

SearchServiceClient.prototype.search = function search(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(SearchService.Search, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.SearchServiceClient = SearchServiceClient;

