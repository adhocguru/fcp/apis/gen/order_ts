// source: v1/order_mobile/order_mobile.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_order_common_pb = require('../../v1/order/common_pb.js');
goog.object.extend(proto, v1_order_common_pb);
var v1_order_enum_pb = require('../../v1/order/enum_pb.js');
goog.object.extend(proto, v1_order_enum_pb);
var v1_order_model_order_pb = require('../../v1/order/model_order_pb.js');
goog.object.extend(proto, v1_order_model_order_pb);
var v1_order_model_product_pb = require('../../v1/order/model_product_pb.js');
goog.object.extend(proto, v1_order_model_product_pb);
goog.exportSymbol('proto.fcp.order.v1.order_mobile.RejectOrderRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_mobile.RejectOrderResponse', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_mobile.SaveOrderRequest', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_mobile.SaveOrderRequest.DetailCase', null, global);
goog.exportSymbol('proto.fcp.order.v1.order_mobile.SaveOrderResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.fcp.order.v1.order_mobile.SaveOrderRequest.oneofGroups_);
};
goog.inherits(proto.fcp.order.v1.order_mobile.SaveOrderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_mobile.SaveOrderRequest.displayName = 'proto.fcp.order.v1.order_mobile.SaveOrderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_mobile.SaveOrderResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_mobile.SaveOrderResponse.displayName = 'proto.fcp.order.v1.order_mobile.SaveOrderResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_mobile.RejectOrderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_mobile.RejectOrderRequest.displayName = 'proto.fcp.order.v1.order_mobile.RejectOrderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.order.v1.order_mobile.RejectOrderResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.order.v1.order_mobile.RejectOrderResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.order.v1.order_mobile.RejectOrderResponse.displayName = 'proto.fcp.order.v1.order_mobile.RejectOrderResponse';
}

/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.oneofGroups_ = [[100,101,102,103]];

/**
 * @enum {number}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.DetailCase = {
  DETAIL_NOT_SET: 0,
  RESOURCES_SEEDS: 100,
  RESOURCES_FERTILIZE: 101,
  RESOURCES_MICRO_FERTILIZE: 102,
  RESOURCES_CHEMISTRY: 103
};

/**
 * @return {proto.fcp.order.v1.order_mobile.SaveOrderRequest.DetailCase}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.getDetailCase = function() {
  return /** @type {proto.fcp.order.v1.order_mobile.SaveOrderRequest.DetailCase} */(jspb.Message.computeOneofCase(this, proto.fcp.order.v1.order_mobile.SaveOrderRequest.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_mobile.SaveOrderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && v1_order_model_order_pb.Order.toObject(includeInstance, f),
    resourcesSeeds: (f = msg.getResourcesSeeds()) && v1_order_model_product_pb.ResourcesSeeds.toObject(includeInstance, f),
    resourcesFertilize: (f = msg.getResourcesFertilize()) && v1_order_model_product_pb.ResourcesFertilize.toObject(includeInstance, f),
    resourcesMicroFertilize: (f = msg.getResourcesMicroFertilize()) && v1_order_model_product_pb.ResourcesMicroFertilizers.toObject(includeInstance, f),
    resourcesChemistry: (f = msg.getResourcesChemistry()) && v1_order_model_product_pb.ResourcesChemistry.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_mobile.SaveOrderRequest;
  return proto.fcp.order.v1.order_mobile.SaveOrderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_order_model_order_pb.Order;
      reader.readMessage(value,v1_order_model_order_pb.Order.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    case 100:
      var value = new v1_order_model_product_pb.ResourcesSeeds;
      reader.readMessage(value,v1_order_model_product_pb.ResourcesSeeds.deserializeBinaryFromReader);
      msg.setResourcesSeeds(value);
      break;
    case 101:
      var value = new v1_order_model_product_pb.ResourcesFertilize;
      reader.readMessage(value,v1_order_model_product_pb.ResourcesFertilize.deserializeBinaryFromReader);
      msg.setResourcesFertilize(value);
      break;
    case 102:
      var value = new v1_order_model_product_pb.ResourcesMicroFertilizers;
      reader.readMessage(value,v1_order_model_product_pb.ResourcesMicroFertilizers.deserializeBinaryFromReader);
      msg.setResourcesMicroFertilize(value);
      break;
    case 103:
      var value = new v1_order_model_product_pb.ResourcesChemistry;
      reader.readMessage(value,v1_order_model_product_pb.ResourcesChemistry.deserializeBinaryFromReader);
      msg.setResourcesChemistry(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_mobile.SaveOrderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_order_model_order_pb.Order.serializeBinaryToWriter
    );
  }
  f = message.getResourcesSeeds();
  if (f != null) {
    writer.writeMessage(
      100,
      f,
      v1_order_model_product_pb.ResourcesSeeds.serializeBinaryToWriter
    );
  }
  f = message.getResourcesFertilize();
  if (f != null) {
    writer.writeMessage(
      101,
      f,
      v1_order_model_product_pb.ResourcesFertilize.serializeBinaryToWriter
    );
  }
  f = message.getResourcesMicroFertilize();
  if (f != null) {
    writer.writeMessage(
      102,
      f,
      v1_order_model_product_pb.ResourcesMicroFertilizers.serializeBinaryToWriter
    );
  }
  f = message.getResourcesChemistry();
  if (f != null) {
    writer.writeMessage(
      103,
      f,
      v1_order_model_product_pb.ResourcesChemistry.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.order.v1.order.Order item = 1;
 * @return {?proto.fcp.order.v1.order.Order}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.order.v1.order.Order} */ (
    jspb.Message.getWrapperField(this, v1_order_model_order_pb.Order, 1));
};


/**
 * @param {?proto.fcp.order.v1.order.Order|undefined} value
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
*/
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional fcp.order.v1.order.ResourcesSeeds resources_seeds = 100;
 * @return {?proto.fcp.order.v1.order.ResourcesSeeds}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.getResourcesSeeds = function() {
  return /** @type{?proto.fcp.order.v1.order.ResourcesSeeds} */ (
    jspb.Message.getWrapperField(this, v1_order_model_product_pb.ResourcesSeeds, 100));
};


/**
 * @param {?proto.fcp.order.v1.order.ResourcesSeeds|undefined} value
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
*/
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.setResourcesSeeds = function(value) {
  return jspb.Message.setOneofWrapperField(this, 100, proto.fcp.order.v1.order_mobile.SaveOrderRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.clearResourcesSeeds = function() {
  return this.setResourcesSeeds(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.hasResourcesSeeds = function() {
  return jspb.Message.getField(this, 100) != null;
};


/**
 * optional fcp.order.v1.order.ResourcesFertilize resources_fertilize = 101;
 * @return {?proto.fcp.order.v1.order.ResourcesFertilize}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.getResourcesFertilize = function() {
  return /** @type{?proto.fcp.order.v1.order.ResourcesFertilize} */ (
    jspb.Message.getWrapperField(this, v1_order_model_product_pb.ResourcesFertilize, 101));
};


/**
 * @param {?proto.fcp.order.v1.order.ResourcesFertilize|undefined} value
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
*/
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.setResourcesFertilize = function(value) {
  return jspb.Message.setOneofWrapperField(this, 101, proto.fcp.order.v1.order_mobile.SaveOrderRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.clearResourcesFertilize = function() {
  return this.setResourcesFertilize(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.hasResourcesFertilize = function() {
  return jspb.Message.getField(this, 101) != null;
};


/**
 * optional fcp.order.v1.order.ResourcesMicroFertilizers resources_micro_fertilize = 102;
 * @return {?proto.fcp.order.v1.order.ResourcesMicroFertilizers}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.getResourcesMicroFertilize = function() {
  return /** @type{?proto.fcp.order.v1.order.ResourcesMicroFertilizers} */ (
    jspb.Message.getWrapperField(this, v1_order_model_product_pb.ResourcesMicroFertilizers, 102));
};


/**
 * @param {?proto.fcp.order.v1.order.ResourcesMicroFertilizers|undefined} value
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
*/
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.setResourcesMicroFertilize = function(value) {
  return jspb.Message.setOneofWrapperField(this, 102, proto.fcp.order.v1.order_mobile.SaveOrderRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.clearResourcesMicroFertilize = function() {
  return this.setResourcesMicroFertilize(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.hasResourcesMicroFertilize = function() {
  return jspb.Message.getField(this, 102) != null;
};


/**
 * optional fcp.order.v1.order.ResourcesChemistry resources_chemistry = 103;
 * @return {?proto.fcp.order.v1.order.ResourcesChemistry}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.getResourcesChemistry = function() {
  return /** @type{?proto.fcp.order.v1.order.ResourcesChemistry} */ (
    jspb.Message.getWrapperField(this, v1_order_model_product_pb.ResourcesChemistry, 103));
};


/**
 * @param {?proto.fcp.order.v1.order.ResourcesChemistry|undefined} value
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
*/
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.setResourcesChemistry = function(value) {
  return jspb.Message.setOneofWrapperField(this, 103, proto.fcp.order.v1.order_mobile.SaveOrderRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderRequest} returns this
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.clearResourcesChemistry = function() {
  return this.setResourcesChemistry(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.order.v1.order_mobile.SaveOrderRequest.prototype.hasResourcesChemistry = function() {
  return jspb.Message.getField(this, 103) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_mobile.SaveOrderResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_mobile.SaveOrderResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderResponse}
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_mobile.SaveOrderResponse;
  return proto.fcp.order.v1.order_mobile.SaveOrderResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_mobile.SaveOrderResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderResponse}
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_mobile.SaveOrderResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_mobile.SaveOrderResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order_mobile.SaveOrderResponse} returns this
 */
proto.fcp.order.v1.order_mobile.SaveOrderResponse.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_mobile.RejectOrderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_mobile.RejectOrderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_mobile.RejectOrderRequest}
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_mobile.RejectOrderRequest;
  return proto.fcp.order.v1.order_mobile.RejectOrderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_mobile.RejectOrderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_mobile.RejectOrderRequest}
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_mobile.RejectOrderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_mobile.RejectOrderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.order.v1.order_mobile.RejectOrderRequest} returns this
 */
proto.fcp.order.v1.order_mobile.RejectOrderRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.order.v1.order_mobile.RejectOrderResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.order.v1.order_mobile.RejectOrderResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.order.v1.order_mobile.RejectOrderResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.RejectOrderResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.order.v1.order_mobile.RejectOrderResponse}
 */
proto.fcp.order.v1.order_mobile.RejectOrderResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.order.v1.order_mobile.RejectOrderResponse;
  return proto.fcp.order.v1.order_mobile.RejectOrderResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.order.v1.order_mobile.RejectOrderResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.order.v1.order_mobile.RejectOrderResponse}
 */
proto.fcp.order.v1.order_mobile.RejectOrderResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.order.v1.order_mobile.RejectOrderResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.order.v1.order_mobile.RejectOrderResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.order.v1.order_mobile.RejectOrderResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.order.v1.order_mobile.RejectOrderResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


goog.object.extend(exports, proto.fcp.order.v1.order_mobile);
