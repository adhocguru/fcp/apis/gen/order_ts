// package: fcp.order.v1.order_mobile
// file: v1/order_mobile/offer_mobile.proto

import * as jspb from "google-protobuf";
import * as v1_order_enum_pb from "../../v1/order/enum_pb";
import * as v1_order_model_offer_pb from "../../v1/order/model_offer_pb";

export class AcceptOfferRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AcceptOfferRequest.AsObject;
  static toObject(includeInstance: boolean, msg: AcceptOfferRequest): AcceptOfferRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AcceptOfferRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AcceptOfferRequest;
  static deserializeBinaryFromReader(message: AcceptOfferRequest, reader: jspb.BinaryReader): AcceptOfferRequest;
}

export namespace AcceptOfferRequest {
  export type AsObject = {
    id: number,
  }
}

export class AcceptOfferResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AcceptOfferResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AcceptOfferResponse): AcceptOfferResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AcceptOfferResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AcceptOfferResponse;
  static deserializeBinaryFromReader(message: AcceptOfferResponse, reader: jspb.BinaryReader): AcceptOfferResponse;
}

export namespace AcceptOfferResponse {
  export type AsObject = {
  }
}

